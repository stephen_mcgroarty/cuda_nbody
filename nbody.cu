
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include "cuda.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

using type = float;

const type pi{ 3.141592653589793 };
const type solar_mass{ 4 * pi * pi };
const type days_per_year{ 365.24 };

template <typename T>
struct planet {
	T x, y, z;
	T vx, vy, vz;
	T mass;
};

template <typename T>
T energy(int nbodies, planet<T> *bodies)
{
	T e = 0.0;
	for (int i = 0; i < nbodies; ++i) 
	{
		planet<T> &b = bodies[i];
		//kinetic energy: 1/2 mass * velocity squared
		e += 0.5 * b.mass * (b.vx * b.vx + b.vy * b.vy + b.vz * b.vz);

		for (int j = i + 1; j < nbodies; j++) 
		{
			planet<T> &b2 = bodies[j];
			T dx = b.x - b2.x;
			T dy = b.y - b2.y;
			T dz = b.z - b2.z;
			T distance = sqrt(dx * dx + dy * dy + dz * dz);
			//potential energy due to gravitational attraction inversely proportional to distance
			//between the two bodies
			e -= (b.mass * b2.mass) / distance;
		}
	}
	return e;
}
template <typename T>
void offset_momentum(int nbodies, planet<T> *bodies)
{
	T px = 0.0, py = 0.0, pz = 0.0;
	for (int i = 0; i < nbodies; ++i) {
		px += bodies[i].vx * bodies[i].mass;
		py += bodies[i].vy * bodies[i].mass;
		pz += bodies[i].vz * bodies[i].mass;
	}
	bodies[0].vx = -px / solar_mass;
	bodies[0].vy = -py / solar_mass;
	bodies[0].vz = -pz / solar_mass;
}

struct planet<type> golden_bodies[5] = {
	{                               /* sun */
		0, 0, 0, 0, 0, 0, solar_mass
	},
	{                               /* jupiter */
		4.84143144246472090e+00,
		-1.16032004402742839e+00,
		-1.03622044471123109e-01,
		1.66007664274403694e-03 * days_per_year,
		7.69901118419740425e-03 * days_per_year,
		-6.90460016972063023e-05 * days_per_year,
		9.54791938424326609e-04 * solar_mass
	},
	{                               /* saturn */
		8.34336671824457987e+00,
		4.12479856412430479e+00,
		-4.03523417114321381e-01,
		-2.76742510726862411e-03 * days_per_year,
		4.99852801234917238e-03 * days_per_year,
		2.30417297573763929e-05 * days_per_year,
		2.85885980666130812e-04 * solar_mass
	},
	{                               /* uranus */
		1.28943695621391310e+01,
		-1.51111514016986312e+01,
		-2.23307578892655734e-01,
		2.96460137564761618e-03 * days_per_year,
		2.37847173959480950e-03 * days_per_year,
		-2.96589568540237556e-05 * days_per_year,
		4.36624404335156298e-05 * solar_mass
	},
	{                               /* neptune */
		1.53796971148509165e+01,
		-2.59193146099879641e+01,
		1.79258772950371181e-01,
		2.68067772490389322e-03 * days_per_year,
		1.62824170038242295e-03 * days_per_year,
		-9.51592254519715870e-05 * days_per_year,
		5.15138902046611451e-05 * solar_mass
	}
};

const type DT{ 1e-2 };
const type RECIP_DT{ 1.0f / DT };
const int range{ 500 };

template <typename T>
void init_random_bodies(int nbodies, planet<T> *bodies)
{
	bodies[ 0 ] = golden_bodies[ 0 ];
	for (int i = 1; i < nbodies; ++i) {
		bodies[ i ].x = ( T )( (rand() % range) - range / 2 );
		bodies[ i ].y = ( T )( ( rand() % range ) - range / 2 );
		bodies[ i ].z = ( T )( ( rand() % range ) - range / 2 );
		bodies[ i ].vx = ( T )( ( rand() % range ) - range / 2 );
		bodies[ i ].vy = ( T )( ( rand() % range ) - range / 2 );
		bodies[ i ].vz = ( T )( ( rand() % range ) - range / 2 );
		bodies[ i ].mass = ( T ) ( (T)( rand() % 11  + 1 ) / 100 )*solar_mass;
	}
}


template<typename T>
__global__ void ScalePlanets(planet<T> *in, T scale)
{
	int index = threadIdx.x;
	in[index].mass *= scale*scale;
	in[index].vx *= scale;
	in[index].vy *= scale;
	in[index].vz *= scale;
}

template<typename T,int nbodies>
__global__ void Advance(planet<T> *in, planet<T> *out, int numIters)
{
	int i =  threadIdx.x;

	planet<T> *read = in;
	planet<T> *write = out;
	planet<T> *swp = nullptr;
	for ( int n = 0; n < numIters; n++ )
	{
		swp = read;
		read = write;
		write = swp;
		for ( int j = 0; j < nbodies; j++ )
		{
			T dx = in[ j ].x - in[ i ].x;
			T dy = in[ j ].y - in[ i ].y;
			T dz = in[ j ].z - in[ i ].z;
			T epsilon = 300;  //softening factor. makes force zero at distance < 1
			T distSqr = dx*dx + dy*dy + dz*dz + epsilon;
			T distSixth = distSqr*distSqr*distSqr;
			T inv_distance = 1.0 / sqrt( distSixth );
			T mag = in[ j ].mass * inv_distance;
			write[ i ].vx += dx *  mag;
			write[ i ].vy += dy *  mag;
			write[ i ].vz += dz *  mag;
		}
		__syncthreads();
		write[ i ].x += write[ i ].vx;
		write[ i ].y += write[ i ].vy;
		write[ i ].z += write[ i ].vz;

	}
}


void initPlanets(planet<type> *planets, int planetCount)
{
	if (planetCount == 5)
		planets = golden_bodies;
	else
	{
		init_random_bodies(planetCount, planets);
	}
}

int main(int argc, char ** argv)
{
	int niters = 1000;
	const int nbodies = 1000;

	if (argc > 1) { niters = atoi(argv[1]); }

	planet<type> *bodies = new planet<type>[nbodies];
	initPlanets(bodies, nbodies);
	

	planet<type> * p_input = nullptr;
	planet<type> * p_deviceOutput = nullptr;


	auto t1 = std::chrono::steady_clock::now();
	offset_momentum(nbodies, bodies);
	type e1 = energy(nbodies, bodies);
	std::cout << std::fixed << std::setprecision(15);
	std::cout << e1 << ' ';

	size_t sizeOfBodies = sizeof(planet<type>)*nbodies;

	cudaMalloc( &p_input , sizeOfBodies );
	cudaMemcpy( p_input , bodies , sizeOfBodies , cudaMemcpyKind::cudaMemcpyHostToDevice );

	cudaMalloc( &p_deviceOutput , sizeOfBodies );
	cudaMemcpy( p_deviceOutput , bodies , sizeOfBodies , cudaMemcpyKind::cudaMemcpyHostToDevice );


	ScalePlanets<type> << < 1 , nbodies >> >( p_input , DT );

	//calculate forces and new velocities
	Advance<type ,  nbodies> << <1 , nbodies >> > ( p_input , p_deviceOutput, niters );
	//swap pointers to use output as input

	ScalePlanets<type> << <1 , nbodies >> >( p_input , RECIP_DT );
	cudaMemcpy( bodies , p_deviceOutput , sizeOfBodies , cudaMemcpyKind::cudaMemcpyDeviceToHost );



	type e2 = energy(nbodies, bodies);
	auto t2 = std::chrono::steady_clock::now();
	auto diff = t2 - t1;

	std::cout << std::fixed << std::setprecision(3);
	std::cout << std::chrono::duration<double>(diff).count() << " ";

	std::cout << std::fixed << std::setprecision(15);
	std::cout << e2 << ' ';


	if (argc != 1)
	{
		delete[] bodies;
	}

	cudaFree( p_input );
	cudaFree( p_deviceOutput );
	return 0;
}
