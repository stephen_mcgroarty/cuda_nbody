#!C:\temp\WinPython-64bit-3.5.1.3Zerobuild1\python-3.5.1.amd64\python.exe
import subprocess
import matplotlib.pyplot as plt

cpuTimes = []
gpuTimes = []

cpuEnergyValues = []
gpuEnergyValues = []
maxItr = 1000
fig = 1
for nbodies in range(400,500,100):
	for iterations in range(10,maxItr,10):
		cpuString = r"C:\Users\b00221295\Desktop\cuda_nbody_cpu\build\Release\nbody.exe"
		cpu = subprocess.Popen([cpuString,str(iterations),str(nbodies)], stdout=subprocess.PIPE,stderr=subprocess.PIPE,stdin=subprocess.PIPE)
		cpuOutput = cpu.communicate()[0].split()

		gpuString = r"C:\Users\b00221295\Desktop\cuda_nbody_gpu\build\Release\nbody_" +str(nbodies) + ".exe"
		gpu = subprocess.Popen([gpuString,str(iterations),str(nbodies)], stdout=subprocess.PIPE,stderr=subprocess.PIPE,stdin=subprocess.PIPE)
		gpuOutput = gpu.communicate()[0].split()

		cpuTimes += [float(cpuOutput[1])]
		gpuTimes += [float(gpuOutput[1])]

		cpuEnergyValues += [float(cpuOutput[2]) - float(cpuOutput[0])]
		gpuEnergyValues += [float(gpuOutput[2]) - float(gpuOutput[0])]
	
plt.figure(1)
plt.plot(cpuTimes,'b',gpuTimes,'g')
plt.ylabel('Time')
plt.xlabel('Iterations (in 10s)')

plt.title('Time to simulate')

plt.show(block=False)

plt.figure(2)
plt.plot(cpuEnergyValues,'b',gpuEnergyValues,'g')
	
plt.ylabel('Energy')
plt.xlabel('Iterations (in 10s)')
plt.title('Diff in energy values for ' + str(nbodies) + ' bodies')

plt.show(block=False)




